#include <iostream>
using namespace std;

int main () {
	
	//Exercise 3 from nested.pdf
	//A pattern I recognized aside from what was shown in lab was that
	//the # of spaces == 4-i (i = row number)
	
	int height;
	cin >> height;
	
	for (int i = 1; i <= height; i++) {
		
		for (int j = 1; j <= 4; j++) {
			//if the column position is less than or equal to the number of spaces. print a space
			if ( j <= (4-i) ) {
				
				cout << " ";
				
			}
			else {
				cout << "*";
			}
						
		}
		
		cout << endl;
	}
	
	return 0;
}
