#include <iostream>
using namespace std;
/*
int main () {

	int number;

	cout << "Enter a number" << endl;
	cin >> number;

	if (number < 10) {
		
		cout << number << endl;

	}
	
	while (number > 10) {
		
		cout << (number % 10) << endl;
		number /= 10;

	}
	cout << number << endl;

	return 0;
}
*/

int main () {

	int number;

	cout << "Enter a positive number" << endl;
	cin >> number;


	if (number < 0) {
		return 0;
	}

	while (number > 10) {
		
		number /= 10;

	}
	cout << number << endl;

	return 0;
}
