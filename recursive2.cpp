using namespace std;
#include <iostream>

bool hasEven(int n) {
	//base case: if n is one digit and even, return true
	if ((n<10) && (n % 2 == 0)) {
		return true;
	}else if ((n<10) && (n%2 != 0)) {
		return false;
	}
	
	if ((n % 10) % 2 == 0) {
		return true;
	}
	hasEven(n/10);
}

int lastEven(int n) {
	
	if (n < 10 && (n%2 == 0)) {
		return n;
	}

	else if ((n % 10) % 2 == 0) {
		return (n % 10);
	}
	else return lastEven(n/10);
}



int main() {
	int n;
	while (cin >> n) {
		cout<< lastEven(n) << endl;
	} 

	return 0;
}
