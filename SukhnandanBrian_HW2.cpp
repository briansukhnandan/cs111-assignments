#include <iostream>
using namespace std;

int main () {
	
	int inches;
	
	cout << "How many inches tall are you?" << endl;
	cin >> inches;
	
	if (inches < 12) {
		cout << "That is 0 feet and " << inches << " inches." << endl;
	}
	else {
		int feet = inches / 12;
		int leftoverInches = inches % 12;
		
		cout << "That is " << feet << " feet and " << leftoverInches << " inches." << endl;		
	}
	
	return 0;
}
