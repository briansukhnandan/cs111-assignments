#include <iostream>
using namespace std;

void reverse(int n) {
	
	int lastDigit;
	
	//Base case:
	//If the number%10 = 0 stop the function.
	//After %10 make sure to divide the number by 10 to keep isolating the last
	//number
	do {
		
		lastDigit = n%10;
		
		if (lastDigit == 0) {
			
			break;
			
		}
		
		cout << lastDigit;
		
		n /= 10;
		
		
	}while (!(lastDigit == 0));
		
}

int main () {
	
	int n;
	
	cout << "Enter a digit that is 10 digits long." << endl;
	cin >> n;
	
	reverse(n);
	
	return 0;
}
