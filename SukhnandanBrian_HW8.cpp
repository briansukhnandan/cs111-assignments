#include <iostream>
#include <string>
using namespace std;

int doubleOdd(int n) {
	
	if (n < 10 && (n % 2) == 0) {
		
		return n;
		
	} 
	else if (n < 10 && (n % 2) != 0) {
		return (n * 11);
	}
	
	if (n % 2 != 0) {
		
		return (doubleOdd(n/10) * 100) + ((n % 10) * 11);
		
	}
	
	return (doubleOdd(n/10)) * 10 + (n%10);
	
}


int main() {
	
	int number;	
		
	cout << "Enter a digit you want to double the odds of." << endl;
	cin >> number;
	cout << doubleOdd(number) << endl;	
	
	return 0;
}

//TODO (RESOLVED)
//Make this recursive.
/*
int doubleOdd(int n) {
	
	string s = to_string(n);
	
	for (int i = s.length() - 1; i >= 0; i--) {
		
		if (s[i] == '1' || s[i] == '3' || s[i] == '5' || s[i] == '7' || s[i] == '9'){
			s.insert(i, 1, s[i]);
		} 
		
		
	}
	
	n = stoi(s);
	
	return n;
}
*/

//new numbers length should be (2*numberOdd) + (numberEven).
