#include <iostream>
using namespace std;

int main () {
	
	int n;

	cout << "Enter a number." << endl;
	cin >> n;
	
	if (n <= 0) {

		cout << "The number you have entered is not valid." << endl;	
		return 0;

	}

	while (n > 0) {
	
		cout << "Hello World!" << endl;
		
		n--;
	}	

	return 0;
}
