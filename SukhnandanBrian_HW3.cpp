#include <iostream>
using namespace std;

int main () {
	
	int number;
	
	cout << "Enter an integer: ";
	cin >> number;
	
	cout << "\n";
	
	//Check to see if number is even
	if (number % 2 == 0) {
		number = number / 2;
	}
	else {
		number = (number * 3) + 1;
	}
	
	cout << "The answer is: " << number << endl;
	
	return 0;
}
