#include <iostream>
using namespace std;

bool validTriangle(int a, int b, int c) {

	if ((a + b) > c &&  (a+c) > b && (b+c) > a) {
		return true;
	}
	else {
		return false;
	}

}

int main () {

	int a, b, c;
	cin >> a >> b >> c;


	if (validTriangle(a, b, c)) cout << "true" << endl;
	else cout << "False" << endl;

	return 0;
}
