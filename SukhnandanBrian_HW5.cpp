#include <iostream>
using namespace std;

int main () {
	
	int number;
	
	cout << "Please enter an integer less than 10." << endl;
	cin >> number;
	
	//Checks if number is valid.
	if (number >= 10) {
		cout << "Invalid number" << endl;
		return 0;
	}
	
	//Keep as a simple name so we don't have to write 100/n a bunch of times.
	int numberofMultiples = (100 / number);
	
	//Declare an array with the size being the number of multiples given the number.
	int multiples[numberofMultiples];
	
	//Store the multiples inside the array.
	for (int i = 0; i < (numberofMultiples); i++) { //declares an array of size that is equal to the number of multiples
		multiples[i] = number * (i + 1);		
	}
		
	for (int j = 0; j < (numberofMultiples); j++) { 
		
		//cout numbers until counter 2 = n. if counter 2 =n, print a new line.
		cout << multiples[j] << " ";
		
		//if j = to a multiple of the number, it should print a new line. this indicates that it will print n numbers on one line.
		//do j + 1 so that in terms of our arithmetic, it will start at one instead of 0.
		if ( ( (j+1) % number) == 0) {		
			cout << endl;		
		} 
		
	}

	return 0;
}
