#include <iostream>
using namespace std;

int main () {
	
	int number;

	cout << "Enter a positive integer!" << endl; cin >> number;
	

	while (number < 0) {

		cout << "That number was not positive, try again!" << endl;
		cin >> number;
	
	}

	if (number > 0) {

		cout << "Thank you! Program terminating . . ." << endl;

	}

	return 0;
}
