#include <iostream>
using namespace std;

int main () {
	
	int number;
	int counter = 0;
	int backwardsCounter = 3;
	int oddCounter;
	
	cout << "Enter a number between 1000 and 1000000. You have 3 attempts." << endl;
	cin >> number;
	
	//Checks validity of the number.
	
	while ( !( (number > 1000) && (number < 1000000) )  ) {
		
		backwardsCounter--;
		
		cout << "That number was invalid, you have " << backwardsCounter << " attempts left." << endl;
		
		cin >> number;
		
		if (backwardsCounter == (0 + 1) ) {
			
			return 0;
			
		}
	
	}
	
	int placeHolder = number;
	
	//Let's try to mod by 10 and checking that number to see if it is odd, then increment another odd number counter.
	//Check last number by modding first, then divide by 10 afterwards, then cycle until numebr < 0.
	
	while (number > 0) {
		//number%10 is the last digit always.
		if ( ( (number % 10) % 2 ) != 0 ) {
			
			oddCounter++;
			
		}
		
		number /= 10;
				
	}
	
	cout << (oddCounter) << " odd numbers." << endl;	
	
	return 0;
}
