#include <iostream>
using namespace std;

int main () {

	string house_number;
	string street;
	string city;
	string state;
	int zipCode;

	cout << "Please enter your House Number: \n";
	cin >> house_number;

	cout << "Please enter Street: \n";
	cin >> street;

	cout << "Please enter County: \n";
	cin >> city;

	cout << "Please enter State: \n";
	cin >> state;

	cout << "Please enter Zip Code: \n";
	cin >> zipCode;

	cout << house_number << " "  << street << "\n" << city << ", " << state << " " << zipCode << endl;

	return 0;
}
